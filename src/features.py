import json

from language import mPesaBad, mPesaGood, loanConfirm, airtimeSpend, loanAmount

SMS_ADDRESS = 'sms_address'
SMS_ADDRESS_KENYA_POWER = 'KenyaPower'
SMS_ADDRESS_MPESA = 'MPESA'
MESSAGE_BODY = 'message_body'

# arguments: smsData, contactData, callData
# Each feature will return a {featureName: featureValue}

# TODO - look at temporal effects
# collect successful history vs unsuccessful history

def mPesaAndLoans(smsLogs, contactLogs, callLogs):
  toReturn = dict()
  priorLoanCount = 0
  mPesaCount = 0
  mPesaBadCount = 0
  mPesaGoodCount = 0
  airSpend = 0
  totalLoansRecorded = 0
  for sms in smsLogs:
    if (sms[SMS_ADDRESS] == SMS_ADDRESS_MPESA):
      mPesaCount = mPesaCount + 1
      mPesaBadCount = mPesaBadCount + mPesaBad(sms[MESSAGE_BODY])
      mPesaGoodCount = mPesaGoodCount + mPesaGood(sms[MESSAGE_BODY])
      airSpend = airSpend + airtimeSpend(sms[MESSAGE_BODY])
    loanConfirmation = loanConfirm(sms[MESSAGE_BODY])
    totalLoansRecorded = totalLoansRecorded +  loanAmount(sms[MESSAGE_BODY])
    if (loanConfirmation):
      priorLoanCount = priorLoanCount + 1

  toReturn['priorLoanCount'] = priorLoanCount
  toReturn['totalLoansRecorded'] = totalLoansRecorded
  toReturn['airtimeSpend'] = airSpend
  toReturn['mPesaBad'] = mPesaBadCount
  toReturn['mPesaCount'] = mPesaCount
  return toReturn

# treating the presence of SMSes between Kenya Power and the user
# currently susceptible to kenya power telling them you're broke and can't afford power.
def recurringElectricity(smsLogs, contactLogs, callLogs):
  toReturn = dict()
  toReturn['hasRecurringElectricity'] = False
  smsCount = 0
  # return presence of kenya power in SMSes.
  for sms in smsLogs:
    if (sms[SMS_ADDRESS] == SMS_ADDRESS_KENYA_POWER):
      smsCount = smsCount + 1
      if smsCount > 1:
        toReturn['hasRecurringElectricity'] = True
  toReturn['electricityContactCount'] = smsCount
  return toReturn

