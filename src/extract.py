import os
import sys
import json
import random

import features
from constants import MESSAGE_BODY_KEY, DATE_TIME_KEY, SMS_ADDRESS_KEY, PHONE_NUMBER_KEY, DURATION_KEY, DISPLAY_NAME_KEY

# TODO - turn into arg. Till then run from root (over src).
dataStartPath = './branch-test-warehouse/uploads/users/'
outputFile = sys.argv[1] if sys.argv[1] != None else './extracted.txt'
contactFolder = 'contact_list'
smsFolder = 'sms_log'
callFolder = 'call_log'
delinquentList = ['70','26','90','35','32','589','689','635','667','364','569','481','493','396','449','561','185','163','174','24','489','189','211','488','5    01','618','615','255','262','266','241','271','265','214','687','242','63','378','473','531','455','302','314','324','331','634','428','607','576','579','696','361','370','367','299','344','369','351'    ,'292','371','366','133','381','232','337','342','176','230','64','391','602','585','603','719','753','732','526','764','822','801','730','837','148','894','820','946','901','960','975','758','1003','    982','970','1013','640','563','1014','1025','1033','1045','1049','1069','1089','1082','1078','357','430','1026','1055','903','1091','1057','1087','1102','1037','963','877','1128','1135','1142','1153',    '1148','1157','1145','1141','1194','1162','1219','1167','1196','1178','776','768','1163','1211','1184','1231','1006','605','1242','633','1023','1073','907','682','855','870','988','1252','1249','1035'    ,'1139','543','917','1066','1171','1000','1188','573','710','841','620','359','1040','1125','1043','1238','1134','1169','1224','948','1140','1302','671','1311','663','1068','285','565','1289','842','1    328','1250','1271','1313','520','1324','1256','139','231','414','800','167','1433','767','1473','1519','1508','1510','1555','1556','1542','1405','735','1561','33','1411','1427','1586','1606','654','12    08','1215','1459','631','1503','1619','708','510','1515','1472','1639','1646','883','1280','1599','1632','587','1436','1622','1572','1520','1133','1710','1637','1553','1593','451','568','1638','599','    1435','1718','1728','1424','1751','1732','1738','516','363','1769','1764','816','718','1744','243','1782','1589','1624','1451','1788','1811','356','1840','1851','1825','1813','1873','1838']

def collapse(data, keys):
  holder = dict()
  for datum in data:
    unique = ''
    for key in keys:
      try:
        unique = unique + str(datum[key])
      except UnicodeEncodeError:
        unique = unique + str(random.randint(0, 10000))
    holder[unique] = datum
  return holder.values()

def main():
  testingFor = [features.mPesaAndLoans,
      features.recurringElectricity]

  userList = [userDir for userDir in os.listdir(dataStartPath) if os.path.isdir(os.path.join(dataStartPath, userDir))]
  nonDelinquentList = list(set(userList) - set(delinquentList))

  if (sys.argv[2] == 'd'):
    userList = delinquentList
  elif (sys.argv[2] == 'nd'):
    userList = nonDelinquentList

  extracted = dict()

  for user in userList:

    phonesDir = os.path.join(dataStartPath, user)
    if os.path.isdir(phonesDir):
      extracted[user] = dict()
      extracted[user]['features'] = list()
      phones = [phone for phone in os.listdir(phonesDir) if os.path.isdir(os.path.join(phonesDir, phone))]
      extracted[user]['phones'] = list()

      for phone in phones:
        extracted[user]['phones'].append(phone)

        smsPath = os.path.join(phonesDir, phone, smsFolder)
        callPath = os.path.join(phonesDir, phone, callFolder)
        contactPath = os.path.join(phonesDir, phone, contactFolder)
        smsData = []
        callData = []
        contactData = []

        if os.path.isdir(smsPath):
          smsFiles = [smsFile for smsFile in os.listdir(smsPath) if not os.path.isdir(os.path.join(smsPath, smsFile))]
          for smsFile in smsFiles:
            try:
              smsData = smsData + json.loads(open(os.path.join(smsPath, smsFile), 'r').read())
            except ValueError:
              print 'Sms json error in file: ' + smsFile
          smsData = collapse(smsData, [MESSAGE_BODY_KEY, SMS_ADDRESS_KEY, DATE_TIME_KEY])
        if os.path.isdir(callPath):
          callFiles = [callFile for callFile in os.listdir(callPath) if not os.path.isdir(os.path.join(callPath, callFile))]
          for callFile in callFiles:
            try:
              callData = callData + json.loads(open(os.path.join(callPath, callFile), 'r').read())
            except ValueError:
              print 'Call json error in file: ' + callFile
          callData = collapse(callData, [PHONE_NUMBER_KEY, DATE_TIME_KEY, DURATION_KEY])
        if os.path.isdir(contactPath):
          contactFiles = [contactFile for contactFile in os.listdir(contactPath) if not os.path.isdir(os.path.join(contactPath, contactFile))]
          for contactFile in contactFiles:
            try:
              contactData = contactData + json.loads(open(os.path.join(contactPath, contactFile), 'r').read())
            except ValueError:
              print 'Contact json error in file: ' + contactFile
          contactData = collapse(contactData, [DISPLAY_NAME_KEY])
          open('asdf', 'w').write(str(contactData))

        # actual work starts here.
        collect = dict()
        for feature in testingFor:
          current = feature(smsData, contactData, callData)
          collect.update(current)
        extracted[user]['features'] = collect
        #print user + ' ' + str(extracted[user]['features'])

  open(sys.argv[1], 'w').write(json.dumps(extracted, indent=4))
  print len(extracted)


if __name__ == "__main__":
  main()

