import re
import regex # non-standard

# Super basic language stuff. Would be delegated to something more accurate in the future.
FAIL = 'fail'
NO_MONEY = 'you do not have enough money'
BAD_CANDIDATES = [FAIL, NO_MONEY]

GOOD_CANDIDATES = []

INVENTURE_LOAN_CONFIRM = 'you should be receiving your loan shortly'
BRANCH_LOAN_CONFIRM = 'branch is now disbursing your loan'
MSHWARI_QUALIFY = 'm-shwari, you now qualify'
LOAD_PRESENCE_CANDIDATES = [INVENTURE_LOAN_CONFIRM, BRANCH_LOAN_CONFIRM, MSHWARI_QUALIFY]

BRANCH_LOAN_REGEX = '(?<=branch is now disbursing your loan of ksh )[0-9]+\,?[0-9]*\.?[0-9]*(?=.*)'
INVENTURE_LOAN_REGEX = '(?<=you have received ksh)[0-9]+\,?[0-9]*\.?[0-9]*(?= from [0-9]* - inventure mobile kenya limited)'
MSHWARI_LOAN_REGEX = '(?<=confirmed\. loan request at m-shwari is approved. new m-pesa balance is ksh)[0-9]+\,?[0-9]*\.?[0-9]*\. your loan of ksh\K[0-9]+\,?[0-9]*\.?[0-9]*'
LOAN_PATTERNS = [BRANCH_LOAN_REGEX, INVENTURE_LOAN_REGEX, MSHWARI_LOAN_REGEX]

AIRTIME_CONFIRMATION_PREFIX = 'you bought ksh'
AIRTIME_CONFIRMATION_SUFFIX = ' of airtime'

def clean(data):
  return data.strip().replace('\r', ' ')

def mPesaBad(data):
  test = data.lower()
  toReturn = 0
  for bad in BAD_CANDIDATES:
    if bad in test:
      toReturn = toReturn + 1
  return toReturn

def mPesaGood(data):
  test = data.lower()
  toReturn = 0
  for good in GOOD_CANDIDATES:
    if good in test:
      toReturn = toReturn + 1
  return toReturn

def airtimeSpend(data):
  test = data.lower()
  start = test.find(AIRTIME_CONFIRMATION_PREFIX)
  end = test.find(AIRTIME_CONFIRMATION_SUFFIX)
  toReturn = 0
  if start != -1 and end != -1:
    spend = test[start + len(AIRTIME_CONFIRMATION_PREFIX):end].replace(',', '')
    toReturn = float(spend)
  return toReturn

def loanConfirm(data):
  test = data.lower()
  toReturn = False
  for loan in LOAD_PRESENCE_CANDIDATES:
    if loan in test:
      toReturn = True
      break
  return toReturn

def loanAmount(data):
  test = clean(data.lower())
  toReturn = 0
  for pattern in LOAN_PATTERNS:
    candidate = regex.search(pattern, test)
    if candidate != None:
      toReturn = float(candidate.group(0).replace(',', ''))
  return toReturn
